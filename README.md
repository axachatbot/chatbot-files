## How to install
1. Double click on the file 'install.command' 
2. The file 'Chatbot Files' should be installed in your Home directory
	- PLEASE DO NOT CHANGE THE LOCATION OF THE FILE

## Instructions for Use (Interactive Learning Interface)

1. Update to latest dialogue-training-data.json
2. Double click on the file 'interactive_learning.command'
3. ... interact with bot ...
4. ... press 0 to save ...
5. Change generated to name that we need
7. Use the latest dialogue-training-data.json

## Instructions for Use (Dialogue Visualisation)

1. Double click on the file 'visualize.command'
2. Image will be saved as grpah.png, you may rename the image as you wish
