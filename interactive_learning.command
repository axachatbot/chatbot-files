#!/bin/bash

interactive_learning () {
	python3 'jsontomd.py'
	python3 'train_online.py'
	python3 'mdtojson.py'
	#echo "Working"
}
cd ~/chatbot-files
if [ -e 'dialogue-training-data.json' ];then
	echo "Is this the latest data file? [y/n]"
	read answer
	if [[ $answer = 'y' ]];then
		echo 'Commencing interactive learning'
		interactive_learning
	else:
		echo "Please download the latest data file and try again."
		kill -INT $$
	fi
else

 	echo "Error: File dialogue-training-data.json does not exist"
 	echo "Please ensure that your dialogue file is named 'dialogue-training-data.json' and that it is in $PWD ."
fi