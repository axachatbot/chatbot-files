from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse

from rasa_nlu import training_data
from rasa_nlu.utils import write_to_file

import json
import os
from rasa_nlu import convert

path_folder = os.path.dirname(os.path.realpath(__file__))
def domain_json_to_yml_string(domain_json):
  # action_factory
  yml_string = 'action_factory: ' + domain_json['action_factory'] + '\n'
  yml_string+= '\n'

  # slots
  try:
    domain_json['slots']
  except KeyError:
    pass
  else:
    slots = domain_json['slots']
    yml_string+='slots:\n'
    for slot_name in list(slots.keys()):
      yml_string+='  '+slot_name+':\n'
      for key in list(slots[slot_name].keys()):
        yml_string+='    '+key+': '+slots[slot_name][key]+'\n'
    yml_string+='\n'

  # entities
  try:
    domain_json['entities']
  except KeyError:
    pass
  else:
    entities = domain_json['entities']
    yml_string+='entities:\n'
    for entity_name in entities:
      yml_string+='  - '+entity_name+'\n'
    yml_string+='\n'

  # intents
  yml_string+='intents:\n'
  intents = domain_json['intents']
  for intent_name in intents:
    yml_string+='  - '+intent_name+'\n'
  yml_string+='\n'

  # actions
  yml_string+='actions:\n'
  actions = domain_json['actions']
  for action_name in actions:
    yml_string+='- '+action_name+'\n'

  return yml_string


def stories_json_to_md_string(stories_json):
  md_string = ''

  # story header
  for story_name in list(stories_json.keys()):
    md_string+= '## '+story_name+'\n'
    story = stories_json[story_name]

    # each story segment
    for segment in story:

      # segment of type intent
      if segment['type'] == 'intent':
        md_string+='* '+segment['intent']
        md_string+=json.dumps(segment['entities'])
        md_string+='\n'

      # segment of type action
      elif segment['type'] == 'action':
        md_string+='  - '+segment['action']+'\n'

      # unable to find a working segment
      else:
        raise ValueError('story segment type must be intent or action.')

    md_string+='\n'

  return md_string

with open(path_folder+'/'+'dialogue-training-data.json') as f:
    json_dict = json.load(f)
    dialogueID = json_dict['_id']
    domain_yml_string = domain_json_to_yml_string(json_dict['data']['domain'])
    stories_md_string = stories_json_to_md_string(json_dict['data']['stories'])

    with open(path_folder+'/'+'stories.md','w') as md:
        md.write(stories_md_string)

    with open(path_folder+'/'+'domain.yml','w') as yml:
        yml.write(domain_yml_string)

    with open(path_folder+'/'+'id.txt','w') as txt:
        txt.write(dialogueID)











