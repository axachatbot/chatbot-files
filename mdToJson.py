import json
import os
import ast

## marker is to differentiate between action and intent segments
def create_story_segment(name, marker, entity):
    dic = {}
    if(marker == '*'):
        dic["type"]='intent'
        dic["intent"]= name
        dic["entities"]= entity
    else:
        dic["type"]='action'
        dic["action"]= name
    return dic

with open('stories.md') as f:

    ######### Dialogue #######
    story = []
    segment = {}
    name = ''
    entity = {}
    intent = ''
    action = ''
    dialogue = {}
    for i in f:
        if('*' in i):
            intentStartPos = 2
            if (i.find('{')!=-1):
                intentEndPos = i.index('{')
                intent = i[int(intentStartPos):int(intentEndPos)]
                entity = ast.literal_eval(i[intentEndPos:])
            else:
                intent = i[2:-1]
            segment = create_story_segment(intent, "*", entity)
            story += [segment,]
            intent = ''
            action = ''
            entity = {}
            
        elif('##' in i):
            if (name != ''):

                dialogue[name] = story
                story = []
            name = i[3:-1]

        elif('-' in i):
            if ("\n" in i):
                action = i.split()[1]
            else:
                action = i.split()[1]
            segment = create_story_segment(action, "-" , entity)
            story += [segment,]
            intent = ''
            action = ''
            entity = {}
    
    dialogue[name] = story ## last story will not be triggered by another story to be added, hence has to be manually added

    ####### Domain #######
    with open('Domain.yml') as g:
        domain = {}
        entity = ''
        entityType = ''
        for i in g:
            if (i.split()== []):
                continue
            elif("action_factory" in i):
                domain['action_factory'] = i[i.index(':')+2:-1]
            
            elif("slots:" in i):
                domain['slots'] = {}
            elif("entities" in i):
                domain["entities"] = []
            elif("intents" in i):
                domain["intents"] = []
            elif("actions" in i):
                domain["actions"] = []

            elif("-" in i):
                name = i[4:-1]
                if("actions" in domain.keys()):
                    name = i[2:-1]
                    domain['actions']+=[name,]
                elif("intents" in domain.keys()):
                    domain["intents"]+= [name,]
                elif("entities" in domain.keys()):
                    domain["entities"] += [name,]

            elif("slots" in domain.keys()):
                if ('type' not in i):
                    entity = i.split()[0][:-1]
                else:
                    entityType = i.split()[1]
                    domain['slots'][entity] = {'type' : entityType}
                    entity = ''
                    entityType = ''

    ####### ID #######
    dialogueID = ''
    with open('id.txt') as k:
        for i in k:
            dialogueID = i

    data = {'_id': dialogueID, 'data':{'domain': domain, 'stories': dialogue}}

    with open("dialogue-training-data.json","w") as h:
        json.dump(data,h, indent = 2)



