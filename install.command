#!/bin/bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install python3
sudo easy_install pip3
cd ~
git clone https://axachatbot@bitbucket.org/axachatbot/chatbot-files.git
python3 -m venv ./
source ./bin/activate
pip3 install rasa_core
pip3 install rasa_nlu
pip3 install graphviz