#!/bin/bash

missing_pygraphviz(){
	echo "Error: Possibly missing pygraphviz"
	echo "Do you want to install pygraphviz? [y/n]"
	read answer
	if [ '$answer' -eq 'y' ];then
		pip3 install pygraphviz
		echo "Trying to run 'visualize.py' again"
		python3 visualize.py || echo 'Failed to run visualize.py'
	else
		echo "Unable to run visualize.py"
	fi
}

python3 visualize.py || missing_pygraphviz
